import pyspark
from pyspark import SparkContext
from pyspark.streaming import StreamingContext

master = 'spark://spark-master:7077'
appName = 'dstream_demo'

# create a streaming context using the variables above, these could be set by spark-submit
sc = SparkContext(master, appName)
ssc = StreamingContext(sc, 10)

# create a DStream from the streaming context, monitor the stream_txt directory
# in cluster mode this will be expected to be in hdfs
dstream = ssc.textFileStream("/stream_txt/")

# define some operations that will happen on the DStream for each batch
mapped = dstream.flatMap(lambda line: line.split())
#count = mapped.count()
#count.pprint()

mapped.foreachRDD(lambda rdd:
        print('words:', rdd.count()))


# start and awaitTermination - Ctrl-C to stop
ssc.start()
ssc.awaitTermination()